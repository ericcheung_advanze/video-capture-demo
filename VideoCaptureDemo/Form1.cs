﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VideoCaptureDemo
{
    public partial class Form1 : Form
    {

        CaptureService captureService;
        public Form1()
        {
            InitializeComponent();
            captureService = new CaptureService(this);
        }

        public void button1_Click(object sender, EventArgs e)
        {
            if(!captureService.IsCameraRunning)
            {
                captureService.StartCapture(loadFileChkBx.Checked);
                this.startStopBtn.Text = "Stop";
            } else
            {
                captureService.StopCapture();
                this.startStopBtn.Text = "Start";
            }
        }

        private void captureBtn_Click(object sender, EventArgs e)
        {
            if(captureService.IsCameraRunning)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "PNG Image|*.png";
                saveFileDialog.ShowDialog();

                if(saveFileDialog.FileName != "")
                {

                    try
                    {
                        System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog.OpenFile();
                        captureService.Image.Save(fs, System.Drawing.Imaging.ImageFormat.Png);

                    } catch(Exception ex)
                    {
                        MessageBox.Show("Cannot save file. Please close the file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            } else
            {
                MessageBox.Show("Please enable capture first!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
