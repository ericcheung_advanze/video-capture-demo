﻿using System.Windows.Forms;

namespace VideoCaptureDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.startStopBtn = new System.Windows.Forms.Button();
            this.captureBtn = new System.Windows.Forms.Button();
            this.loadFileChkBx = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(776, 353);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // startStopBtn
            // 
            this.startStopBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.startStopBtn.Location = new System.Drawing.Point(290, 386);
            this.startStopBtn.Name = "startStopBtn";
            this.startStopBtn.Size = new System.Drawing.Size(75, 34);
            this.startStopBtn.TabIndex = 1;
            this.startStopBtn.Text = "Start";
            this.startStopBtn.UseVisualStyleBackColor = true;
            this.startStopBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // captureBtn
            // 
            this.captureBtn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.captureBtn.Location = new System.Drawing.Point(426, 386);
            this.captureBtn.Name = "captureBtn";
            this.captureBtn.Size = new System.Drawing.Size(93, 34);
            this.captureBtn.TabIndex = 2;
            this.captureBtn.Text = "Capture";
            this.captureBtn.UseVisualStyleBackColor = true;
            this.captureBtn.Click += new System.EventHandler(this.captureBtn_Click);
            // 
            // loadFileChkBx
            // 
            this.loadFileChkBx.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.loadFileChkBx.AutoSize = true;
            this.loadFileChkBx.Location = new System.Drawing.Point(566, 392);
            this.loadFileChkBx.Name = "loadFileChkBx";
            this.loadFileChkBx.Size = new System.Drawing.Size(131, 24);
            this.loadFileChkBx.TabIndex = 4;
            this.loadFileChkBx.Text = "Load from file";
            this.loadFileChkBx.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.loadFileChkBx);
            this.Controls.Add(this.captureBtn);
            this.Controls.Add(this.startStopBtn);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Button startStopBtn;
        private Button captureBtn;
        private CheckBox loadFileChkBx;

        public PictureBox PictureBox1 { get => pictureBox1; set => pictureBox1 = value; }
    }
}

