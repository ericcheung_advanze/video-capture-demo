﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace VideoCaptureDemo
{
    class CaptureService
    {

        static byte[] MFX_PLUGINID_HEVCE_SW = new byte[] { 0x2f, 0xca, 0x99, 0x74, 0x9f, 0xdb, 0x49, 0xae, 0xb1, 0x21, 0xa5, 0xb6, 0x3e, 0xf5, 0x68, 0xf7 };
        static byte[] MFX_PLUGINID_HEVCE_HW = new byte[] { 0x6f, 0xad, 0xc7, 0x91, 0xa0, 0xc2, 0xeb, 0x47, 0x9a, 0xb6, 0xdc, 0xd5, 0xea, 0x9d, 0xa3, 0x47 };
        static byte[] MFX_PLUGINID_HEVCE_GACC = new byte[] { 0xe5, 0x40, 0x0a, 0x06, 0xc7, 0x4d, 0x41, 0xf5, 0xb1, 0x2d, 0x43, 0x0b, 0xba, 0xa2, 0x3d, 0x0b };


        private VideoCapture capture;
        private VideoWriter writer;

        private FFMpegEncoder fFMpegEncoder;

        private Thread camera;
        private Form1 form;

        public bool IsCameraRunning { get; set; } = false;
        public Mat Frame { get; set; }
        public Bitmap Image { get; set; }
 
        int width, height;
        OpenCvSharp.Size sz;

        private bool IsFile = false;
        private string FileName = "ff.avi";

        public void StartCapture(bool IsFile = false)
        {
            this.IsFile = IsFile;
            Console.WriteLine("LOAD FROM FILE: " + this.IsFile);
            IsCameraRunning = true;
            camera = new Thread(new ThreadStart(CaptureCameraCallback));
            camera.Start();
            sz = new OpenCvSharp.Size(width, height);
            if(!IsFile)
            {
                writer = new VideoWriter();
                writer.Open(FileName, FourCC.DIVX, 30, sz);
                if (!writer.IsOpened())
                {
                    throw new NullReferenceException();
                }
                /*
                 * HARDWARE ENCODER BELOW
                 */
                //fFMpegEncoder = new FFMpegEncoder(10000, 30, FileName);
                //fFMpegEncoder.Start();
            }
            
        }

        public void StopCapture()
        {
                IsCameraRunning = false;
                capture.Release();
                if (!IsFile)
                {
                    writer?.Release();
                    //fFMpegEncoder.Close();
                }

                writer?.Dispose();
        }

        public CaptureService(Form1 form)
        {
            this.form = form;

            width = 1280;
            height = 720;
        }

        public static byte[] convertImageToBytes(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        private void Display()
        {
            if(form.PictureBox1.InvokeRequired)
            {
                form.PictureBox1.Invoke(new MethodInvoker(() => {
                    this.Display();
                }));
            } else
            {
                if (form.PictureBox1.Image != null)
                {
                    form.PictureBox1.Image.Dispose();
                }
                form.PictureBox1.Image = Image;
            }
        }

        private void CaptureCameraCallback()
        {
            Frame = new Mat();
            capture = IsFile ?  new VideoCapture(FileName) : new VideoCapture(0);
            if(!IsFile) capture.Open(0);

            int sleepTime = (int)Math.Round(1000 / capture.Fps);

            if (capture.IsOpened() || IsFile)
            {
                while(IsCameraRunning)
                {
                    capture.Read(Frame);
                    if(IsFile && Frame.Empty())
                    {
                        form.Invoke(new MethodInvoker(() => { form.button1_Click(this, null); }));
                        break;
                    }
                    Image = BitmapConverter.ToBitmap(Frame);
                    Bitmap outImage = (Bitmap) Image.Clone();
                    this.Display();
                    Mat resized = Frame.Resize(new OpenCvSharp.Size(width, height));
                    Image = BitmapConverter.ToBitmap(resized);
                    if (!IsFile)
                    {
                        if(!writer.IsDisposed)
                        {
                            writer?.Write(resized);
                        } else
                        {
                            Console.WriteLine("WARN: IS DISPOSED!!!!");
                        }
                            
                        //fFMpegEncoder.Write(outImage);
                    }
                    else {
                        Thread.Sleep(sleepTime);
                    };
                }
            }
        }
    }
}