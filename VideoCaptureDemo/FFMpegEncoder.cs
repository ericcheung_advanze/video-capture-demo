﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoCaptureDemo
{
    class FFMpegEncoder
    {
        Process proc;
        bool started = false;

        public FFMpegEncoder(uint bitrate, uint fps, string outputFileName)
        {
            proc = new Process();
            proc.StartInfo.FileName = @"ffmpeg.exe";
            //proc.StartInfo.Arguments = String.Format("-f image2pipe -framerate {1} -i - -load_plugin hevc_hw -c:v hevc_qsv -maxrate {0}k -an -y {2}",
            //    bitrate, fps, outputFileName);
            proc.StartInfo.Arguments = String.Format("-f image2pipe -framerate {1} -i - -c:v h264_qsv -maxrate {0}k -an -y {2}",
                bitrate, fps, outputFileName);
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
        }

        public void Start()
        {
            proc.Start();
            started = true;
        }

        public void Write(Bitmap image)
        {
            if (!started) return;
            using (var ms = new MemoryStream())
            {
                using (image)
                {
                    image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.WriteTo(proc.StandardInput.BaseStream);
                }
            }
        }

        public void Close()
        {
            started = false;
            proc.CloseMainWindow();
            proc.Close();
            //proc.WaitForExit(10000);
            proc.Dispose();
            
        }
    }
}
